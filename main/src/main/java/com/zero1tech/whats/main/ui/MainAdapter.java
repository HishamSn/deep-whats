package com.zero1tech.whats.main.ui;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.ads.formats.MediaView;
import com.google.android.gms.ads.formats.UnifiedNativeAdView;
import com.zero1tech.whats.main.R;
import com.zero1tech.whats.main.databinding.RowCardAdsBinding;
import com.zero1tech.whats.main.databinding.RowMainBinding;
import com.zero1tech.whats.main.model.MainModel;

import java.util.ArrayList;
import java.util.List;

import static com.zero1tech.whats.main.util.ActivityUtil.startActivityCode;


public class MainAdapter extends RecyclerView.Adapter<MainAdapter.ViewHolder> {

    private final static int ROW_UNIFIED_NATIVE_AD = R.layout.row_card_ads;
    private final static int ROW_MAIN = R.layout.row_main;
    private List<MainModel> mainList = new ArrayList<>();
    private Context context;

    public MainAdapter(List<MainModel> mainList) {

        this.mainList = mainList;
    }

    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
        context = recyclerView.getContext();
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 3) {
            return ROW_UNIFIED_NATIVE_AD;
        }
        return ROW_MAIN;
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(viewType, parent, false);
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());

        if (viewType == ROW_MAIN) {
            RowMainBinding binding = RowMainBinding.inflate(layoutInflater, parent, false);
            return new ViewHolder(binding);
        } else if (viewType == ROW_UNIFIED_NATIVE_AD) {
            RowCardAdsBinding binding1 = RowCardAdsBinding.inflate(layoutInflater, parent, false);
            return new ViewHolder(binding1);
        } else {
            throw new IllegalStateException("Unexpected value: " + viewType);
        }


    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        if (position == 3) {
//            holder.rowCardAdsBinding.

            return;
        }

        MainModel mainChatModel = mainList.get(position);

        holder.bind(mainChatModel);
        holder.rowMainBinding.ivIcon.setImageResource(mainChatModel.getIcon());
        holder.itemView.setOnClickListener(view -> {
            startActivityCode(context, MainActivity.class, mainChatModel.getTransitionActivity());
        });
    }

    @Override
    public int getItemCount() {
        return mainList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        private RowMainBinding rowMainBinding;
        private RowCardAdsBinding rowCardAdsBinding;

        ViewHolder(RowMainBinding binding) {
            super(binding.getRoot());
            this.rowMainBinding = binding;
        }

        ViewHolder(RowCardAdsBinding binding) {
            super(binding.getRoot());
            this.rowCardAdsBinding = binding;
        }

        public void bind(MainModel mainChatModel) {
            rowMainBinding.setMainModel(mainChatModel);
            rowMainBinding.executePendingBindings();
        }


    }

    public class UnifiedNativeAdViewHolder extends RecyclerView.ViewHolder {

        private UnifiedNativeAdView adView;

        public UnifiedNativeAdView getAdView() {
            return adView;
        }

        UnifiedNativeAdViewHolder(View view) {
            super(view);
            adView = (UnifiedNativeAdView) view.findViewById(R.id.ad_view);

            // The MediaView will display a video asset if one is present in the ad, and the
            // first image asset otherwise.
            adView.setMediaView((MediaView) adView.findViewById(R.id.ad_media));

            // Register the view used for each individual asset.
            adView.setHeadlineView(adView.findViewById(R.id.ad_headline));
            adView.setBodyView(adView.findViewById(R.id.ad_body));
            adView.setCallToActionView(adView.findViewById(R.id.ad_call_to_action));
            adView.setIconView(adView.findViewById(R.id.ad_icon));
            adView.setPriceView(adView.findViewById(R.id.ad_price));
            adView.setStarRatingView(adView.findViewById(R.id.ad_stars));
            adView.setStoreView(adView.findViewById(R.id.ad_store));
            adView.setAdvertiserView(adView.findViewById(R.id.ad_advertiser));
        }
    }
}
