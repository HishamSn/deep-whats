package com.zero1tech.whats.main.ui;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;

import com.google.android.gms.ads.AdView;
import com.zero1tech.whats.core.BuildConfig;
import com.zero1tech.whats.core.activities.BaseActivity;
import com.zero1tech.whats.main.R;
import com.zero1tech.whats.main.databinding.ActivityMainBinding;
import com.zero1tech.whats.main.model.MainModel;

import java.util.ArrayList;
import java.util.List;

import static com.zero1tech.whats.core.util.AdsUtil.initBannerAd;
import static com.zero1tech.whats.main.util.ActivityUtil.DIRECT_CHAT_ACTIVITY;
import static com.zero1tech.whats.main.util.ActivityUtil.STATUS_SAVER_ACTIVITY;
import static com.zero1tech.whats.main.util.ActivityUtil.WEB_WHATSAPP_ACTIVITY;


public class MainActivity extends BaseActivity {


    ActivityMainBinding binding;
    private List<MainModel> mainList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        fillList();
        binding.rvMain.setAdapter(new MainAdapter(mainList));
        initAds(this);


    }

    private void initAds(Context ctx) {
        if (!BuildConfig.isAdsBlocked) {
            AdView adView = new AdView(ctx);
            initBannerAd(adView, findViewById(R.id.container_ad_view), BuildConfig.MAIN_BANNER_ID);
            Log.d("MAIN_BANNER_ID", BuildConfig.MAIN_BANNER_ID);
        }
    }

    public void fillList() {
        mainList = new ArrayList<>();

        mainList.add(new MainModel(R.string.web_whats, R.drawable.ic_web_whats, WEB_WHATSAPP_ACTIVITY));
        mainList.add(new MainModel(R.string.status_saver, R.drawable.ic_download, STATUS_SAVER_ACTIVITY));
        mainList.add(new MainModel(R.string.direct_chat, R.drawable.ic_direct_chat, DIRECT_CHAT_ACTIVITY));
//        mainList.add(new MainModel(R.string.sticker_builder, R.drawable.ic_whatsapp, WEB_WHATSAPP_ACTIVITY));
//        mainList.add(new MainModel(R.string.whats_cleaner, R.drawable.ic_whatsapp, WEB_WHATSAPP_ACTIVITY));
//        mainList.add(new MainModel(R.string.fake_chat, R.drawable.ic_whatsapp, WEB_WHATSAPP_ACTIVITY));
//        mainList.add(new MainModel(R.string.recover_deleted_message, R.drawable.ic_whatsapp, WEB_WHATSAPP_ACTIVITY));
//        mainList.add(new MainModel(R.string.convert_gif, R.drawable.ic_whatsapp, WEB_WHATSAPP_ACTIVITY));
//        mainList.add(new MainModel(R.string.text_to_emoji, R.drawable.ic_whatsapp, WEB_WHATSAPP_ACTIVITY));
//        mainList.add(new MainModel(R.string.whats_gallery, R.drawable.ic_whatsapp, WEB_WHATSAPP_ACTIVITY));

    }
}
