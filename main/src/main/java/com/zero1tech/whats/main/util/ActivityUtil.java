package com.zero1tech.whats.main.util;

import android.content.Context;
import android.content.Intent;

import androidx.annotation.IntDef;

import com.zero1tech.whats.directchat.DirectChatActivity;
import com.zero1tech.whats.statussaver.ui.StatusSaverActivity;
import com.zero1tech.whats.webwhatsapp.WebViewActivity;


public class ActivityUtil {

    public static final int MAIN_ACTIVITY = 0;
    public static final int DIRECT_CHAT_ACTIVITY = 1;
    public static final int WEB_WHATSAPP_ACTIVITY = 2;
    public static final int STATUS_SAVER_ACTIVITY = 3;
    public static final int BBM_ACTIVITY = 4;

    @IntDef({
            MAIN_ACTIVITY, DIRECT_CHAT_ACTIVITY, WEB_WHATSAPP_ACTIVITY, STATUS_SAVER_ACTIVITY,
            BBM_ACTIVITY
    })
    public @interface IntentType {
    }


    public static void startActivityCode(Context context, Class defaultClass, @IntentType int transitionActivity) {
        startActivityCode(context, defaultClass, transitionActivity, false);
    }


    public static void startActivityCode(Context context, Class defaultClass, @IntentType int transitionActivity, boolean isClear) {
        Intent intent = null;

        switch (transitionActivity) {

            default:
                intent = new Intent(context, defaultClass);
                break;

            case STATUS_SAVER_ACTIVITY:
                intent = new Intent(context, StatusSaverActivity.class);
                break;

            case WEB_WHATSAPP_ACTIVITY:

                intent = new Intent(context, WebViewActivity.class);
                break;

            case DIRECT_CHAT_ACTIVITY:
                intent = new Intent(context, DirectChatActivity.class);
                break;

            case BBM_ACTIVITY:
//                intent = new Intent(context, BbmActivity.class);
                break;

            case MAIN_ACTIVITY:
//                intent = new Intent(context, MainActivity.class);
                break;

        }
        context.startActivity(intent);

//        ((Activity) context).finish();

    }
}
