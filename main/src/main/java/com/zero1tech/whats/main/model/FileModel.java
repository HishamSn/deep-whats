package com.zero1tech.whats.main.model;


public class FileModel {
    private String path;
    private boolean isSelected = false;

    public FileModel(String path) {
        this.path = path;
    }

    public String getPath() {
        return path;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }


    public boolean isSelected() {
        return isSelected;
    }

}
