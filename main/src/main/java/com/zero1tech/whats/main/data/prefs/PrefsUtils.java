package com.zero1tech.whats.main.data.prefs;


import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.zero1tech.whats.core.MyApplication;


public class PrefsUtils {

    private static final String LOGIN = "login";
    private static final String LANGUAGE_SELECTED = "language_selected";
    private static final String FIRST_TIME_OPEN = "FIRST_TIME_OPEN";
    private static PrefsUtils instance;
    private SharedPreferences prefs;


    private PrefsUtils() {
        prefs = PreferenceManager.getDefaultSharedPreferences(MyApplication.getInstance());
    }

    public static synchronized PrefsUtils getInstance() {
        if (instance == null) {
            instance = new PrefsUtils();
        }
        return instance;
    }

    public boolean isLogin() {
        return prefs.getBoolean(LOGIN, false);
    }

    public void setLogin(boolean login) {
        prefs.edit().putBoolean(LOGIN, login).apply();
    }

    public boolean isFirstTimeOpen() {
        return prefs.getBoolean(FIRST_TIME_OPEN, true);
    }

    public void setFirstTimeOpen(boolean login) {
        prefs.edit().putBoolean(FIRST_TIME_OPEN, login).apply();
    }



}
