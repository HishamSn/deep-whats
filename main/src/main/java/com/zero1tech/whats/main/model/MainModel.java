package com.zero1tech.whats.main.model;

import androidx.annotation.DrawableRes;
import androidx.annotation.StringRes;

public class MainModel {

    private int text;
    private int icon;
    private int transitionActivity;

    public MainModel(@StringRes int text, @DrawableRes int icon, int transitionActivity) {
        this.text = text;
        this.icon = icon;
        this.transitionActivity = transitionActivity;
    }

    public int getText() {
        return text;
    }

    public void setText(int text) {
        this.text = text;
    }

    public int getIcon() {
        return icon;
    }

    public void setIcon(int icon) {
        this.icon = icon;
    }

    public int getTransitionActivity() {
        return transitionActivity;
    }

    public void setTransitionActivity(int transitionActivity) {
        this.transitionActivity = transitionActivity;
    }
}
