package com.zero1tech.whats.main.constant;

public class AppConstant {
    public static final String PACKAGE = "com.zero1tech.wpsaver";
    public static final String PATH_BWP_STATUSES = "/WhatsApp Business/Media/.Statuses/";
    public static final String PATH_WP_STATUSES = "/WhatsApp/Media/.Statuses/";
    public static final String PACKAGE_NAME = "com.zero1tech.wpsaver";

    public static final int REQUEST_WRITE_EXTERNAL_STORAGE = 1;
    public static final String EXTRA_VIDEO_VIEWER = "VIDEO_VIEWER";
    public static final String EXTRA_IMAGE_VIEWER = "IMAGE_VIEWER";


    public static final String SELECTED_LANGUAGE = "Locale.Language";
    public static final String AR = "ar";
    public static final String EN = "en";
}
