package com.zero1tech.whats.statussaver.ui.fragments.wa;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.viewpager2.widget.ViewPager2;

import com.google.android.material.tabs.TabLayout;
import com.google.android.material.tabs.TabLayoutMediator;
import com.zero1tech.whats.statussaver.R;


public class MainFragment extends Fragment {

    private ViewPager2 viewPager;
    private TabLayout tabLayout;

    public MainFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_main, container, false);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

//        MainViewPager2Adapter mainViewPager2Adapter = new MainViewPager2Adapter(this);
        viewPager = view.findViewById(R.id.viewPager_wa);
//        viewPager.setAdapter(mainViewPager2Adapter);


        tabLayout = view.findViewById(R.id.tab_layout_wa);

        new TabLayoutMediator(tabLayout, viewPager, (tab, position) -> {
            if (position == 0) {
                tab.setText(R.string.Images);
            } else if (position == 1) {
                tab.setText(R.string.Videos);
            }
        }).attach();

        viewPager.setOffscreenPageLimit(2);

    }
}
