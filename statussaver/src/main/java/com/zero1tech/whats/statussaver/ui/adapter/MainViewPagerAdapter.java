package com.zero1tech.whats.statussaver.ui.adapter;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.viewpager2.adapter.FragmentStateAdapter;

import com.zero1tech.whats.statussaver.ui.fragments.wa.WAImage2Fragment;
import com.zero1tech.whats.statussaver.ui.fragments.wa.WAVideo2Fragment;


public class MainViewPagerAdapter extends FragmentStateAdapter {


    public MainViewPagerAdapter(@NonNull FragmentActivity fragmentActivity) {
        super(fragmentActivity);
    }

    @NonNull
    @Override
    public Fragment createFragment(int position) {
        if (position == 0) {
            return new WAImage2Fragment();
        } else if (position == 1) {
            return new WAVideo2Fragment();
        }
        return null;
    }

    @Override
    public int getItemCount() {
        return 2;
    }
}