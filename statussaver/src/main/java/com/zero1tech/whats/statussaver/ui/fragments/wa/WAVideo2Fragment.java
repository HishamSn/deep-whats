package com.zero1tech.whats.statussaver.ui.fragments.wa;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.view.ActionMode;
import androidx.appcompat.widget.AppCompatButton;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.ads.InterstitialAd;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.zero1tech.whats.core.BuildConfig;
import com.zero1tech.whats.core.activities.BaseActivity;
import com.zero1tech.whats.statussaver.R;
import com.zero1tech.whats.statussaver.model.fileModel;
import com.zero1tech.whats.statussaver.ui.adapter.WAVideoAdapter;
import com.zero1tech.whats.statussaver.ui.viewer.ViewerActivity;
import com.zero1tech.whats.statussaver.util.HelperMethods;
import com.zero1tech.whats.statussaver.util.RecyclerClickListener;
import com.zero1tech.whats.statussaver.util.RecyclerTouchListener;

import java.io.File;
import java.util.ArrayList;

import butterknife.ButterKnife;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;
import static com.zero1tech.whats.core.util.AdsUtil.initInterstitialAd;
import static com.zero1tech.whats.statussaver.constant.AppConstant.EXTRA_VIDEO_VIEWER;
import static com.zero1tech.whats.statussaver.constant.AppConstant.PATH_WP_STATUSES;
import static com.zero1tech.whats.statussaver.util.DialogUtil.loadDialogHelp;
import static com.zero1tech.whats.statussaver.util.FileUtil.fillVideoArray;
import static com.zero1tech.whats.statussaver.util.FileUtil.getFiles;
import static com.zero1tech.whats.statussaver.util.FileUtil.saveAllFiles;
import static com.zero1tech.whats.statussaver.util.FileUtil.sort;

/**
 * A simple {@link Fragment} subclass.
 */
public class WAVideo2Fragment extends Fragment implements ActionMode.Callback {
    private RecyclerView rvFiles;
    private ProgressBar progressBar;
    private FloatingActionButton fab;
    private FrameLayout fragmentWaImage;

    private LinearLayout layoutEmpty;
    private AppCompatButton btnOpenWhats;

    private WAVideo2Fragment instance;
    private WAVideoAdapter videoAdapter;
    private ArrayList<fileModel> fileModelList = new ArrayList<>();
    private View view;
    private ActionMode actionMode;
    private File[] listFiles;

    private InterstitialAd interstitialAd;
//    private ToolbarActionModeCallback actionModeCallback;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_wa_image, container, false);
        ButterKnife.bind(this, view);
        instance = this;
        rvFiles = view.findViewById(R.id.recyclerview_wa_image);
        progressBar = view.findViewById(R.id.progressbar_wa);
        fab = view.findViewById(R.id.wa_image_fab_save_all);
        fragmentWaImage = view.findViewById(R.id.fragment_wa_image);
        layoutEmpty = view.findViewById(R.id.layout_empty);
        btnOpenWhats = view.findViewById(R.id.btn_open_whatsapp);


        fab.setOnClickListener(view -> {
            saveAll();
            if (interstitialAd.isLoaded()) {
//                    interstitialAd.show();
            } else {
                Log.d("TAG", "The interstitial wasn't loaded yet.");
            }
        });
        btnOpenWhats.setOnClickListener(view -> {
            Intent launchIntent = getActivity().getPackageManager().getLaunchIntentForPackage("com.whatsapp");
            if (launchIntent == null) {
                launchIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + "com.whatsapp"));
            }
            startActivity(launchIntent);
        });

        populateRecyclerView();
        initRecyclerViewClickListeners();
        initAds(getContext());
        getFileStatus();

//        actionModeCallback = new ToolbarActionModeCallback(getActivity()
//                , new GenericAdapter<>(videoAdapter)
//                , fileModelList, new InstanceHandler<>(instance));

        return view;
    }

    private void initAds(Context ctx) {
        if (!BuildConfig.isAdsBlocked) {
            interstitialAd = initInterstitialAd(ctx, interstitialAd);
        }
    }

    private void populateRecyclerView() {
        rvFiles.setHasFixedSize(true);

        videoAdapter = new WAVideoAdapter(fileModelList);
        rvFiles.setAdapter(videoAdapter);

        progressBar.setVisibility(GONE);
    }

    //Implement item click and long click over recycler view
    private void initRecyclerViewClickListeners() {
        rvFiles.addOnItemTouchListener(new RecyclerTouchListener(
                rvFiles, new RecyclerClickListener() {
            @Override
            public void onClick(View view, int position) {

                if (position == fileModelList.size() - 1) {
                    loadDialogHelp(getContext());
                    return;
                }
                //If ActionMode not null select item
                if (actionMode != null)
                    onListItemSelect(position);
                else {
                    Intent intent = new Intent(getActivity(), ViewerActivity.class);
                    intent.putExtra("path", videoAdapter.getItem(position).getPath());
                    intent.putExtra("type", EXTRA_VIDEO_VIEWER);
                    startActivityForResult(intent, 101);
                }
            }

            @Override
            public void onLongClick(View view, int position) {
                //Select item on long click
                if (position == fileModelList.size() - 1) {
                    loadDialogHelp(getContext());
                    return;
                }

                actionMode = null;
                onListItemSelect(position);
            }
        }));
    }


    //List item select method
    private void onListItemSelect(int position) {
        videoAdapter.toggleSelection(position);//Toggle the selection

        boolean hasCheckedItems = videoAdapter.getSelectedCount() > 0;//Check if any items are already selected or not

        if (hasCheckedItems && actionMode == null) {
            // there are some selected items, start the actionMode
            BaseActivity activity = (BaseActivity) getActivity();

            actionMode = activity.startSupportActionMode(this);

        } else if (!hasCheckedItems && actionMode != null) {
            actionMode.finish();
            actionMode = null;
        }

        if (actionMode != null)
            //set action mode title on item selection
            actionMode.setTitle(videoAdapter.getSelectedCount() + " "
                    + getString(R.string.space_selected));

    }

    //Set action mode null after use
    public void setNullToActionMode() {
        if (actionMode != null)
            actionMode = null;
    }

    @Override
    public void onResume() {
        refresh();
        super.onResume();
    }

    private void saveAll() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
        alertDialogBuilder.setTitle(R.string.title_save_all_files);
        alertDialogBuilder
                .setMessage(R.string.action_save_all_files)
                .setCancelable(false)
                .setPositiveButton(R.string.yes, (dialog, id) -> {
                    // if this button is clicked, close
                    if (listFiles.length == 0) {
                        Toast.makeText(getActivity(), R.string.no_status_available, Toast.LENGTH_SHORT).show();
                    } else {
                        saveAllFiles(getContext(), listFiles, isComplete -> {
                            Toast.makeText(getActivity(), R.string.saved_success, Toast.LENGTH_SHORT).show();
                        });
                    }
                })
                .setNegativeButton(R.string.No, (dialog, id) -> {
                    dialog.cancel();
                });


        // create alert dialog
        final AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.setOnShowListener(arg0 -> {
            alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.black_overlay));
            alertDialog.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(getResources().getColor(R.color.black_overlay));
        });

        // show it
        alertDialog.show();
    }


    private void getFileStatus() {
        listFiles = getFiles(PATH_WP_STATUSES);
        if (listFiles == null) {
            Toast.makeText(getContext(), "This Version of android not supported", Toast.LENGTH_SHORT).show();
            return;
        }
        sort(listFiles);
        if (listFiles.length != 0) {
            fillVideoArray(listFiles, fileModelList);
            videoAdapter.notifyDataSetChanged();
        }


        if (fileModelList.isEmpty()) {
            viewVisibility(VISIBLE, GONE, GONE);
        } else {
            viewVisibility(GONE, VISIBLE, VISIBLE);
        }
    }

    private void viewVisibility(int layoutEmptyVisible, int fabVisible, int rvVisible) {
        layoutEmpty.setVisibility(layoutEmptyVisible);
        fab.setVisibility(GONE);
        rvFiles.setVisibility(rvVisible);
    }

    public void deleteRows() {
        SparseBooleanArray selected = videoAdapter.getSelectedIds();//Get selected ids

        //Loop all selected ids
        for (int i = (selected.size() - 1); i >= 0; i--) {
            if (selected.valueAt(i)) {
                //If current id is selected remove the item via key
                fileModelList.remove(selected.keyAt(i));
                videoAdapter.notifyDataSetChanged();//notify adapter

            }
        }
        Toast.makeText(getActivity(), selected.size() + getString(R.string.itemDeleted), Toast.LENGTH_SHORT).show();//Show Toast
        actionMode.finish();//Finish action mode after use

    }

    public void refresh() {
        if (this.actionMode != null) {
            this.actionMode.finish();
        }
        videoAdapter.updateData(new ArrayList<>());
        getFileStatus();
//        swipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 1 && data != null) {
            if (resultCode == -1) {
                refresh();
            }
            if (resultCode == Activity.RESULT_CANCELED) {
                refresh();
            }
        }
    }

    public InterstitialAd getInterstitialAd() {
        return interstitialAd;
    }


    @Override
    public boolean onCreateActionMode(ActionMode mode, Menu menu) {
        mode.getMenuInflater().inflate(R.menu.selection_menu, menu);//Inflate the menu_main over action mode

        return true;
    }

    @Override
    public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
        menu.findItem(R.id.menu_delete).setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
        menu.findItem(R.id.menu_save).setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);

        return true;
    }

    @Override
    public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
        SparseBooleanArray selectedIds;
        int size;

        int itemId = item.getItemId();
        if (itemId == R.id.menu_delete) {
            selectedIds = videoAdapter.getSelectedIds();
            for (size = selectedIds.size() - 1; size >= 0; size--) {
                if (selectedIds.valueAt(size)) {
                    String str = videoAdapter.getItem(selectedIds.keyAt(size)).getPath();
                    File file = new File(str);
                    try {
                        if (file.exists() && file.isFile()) {
                            file.delete();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
            deleteRows();
            refresh();
            mode.finish();
            return true;
        } else if (itemId == R.id.menu_save) {
            selectedIds = videoAdapter.getSelectedIds();
            for (size = selectedIds.size() - 1; size >= 0; size--) {
                if (selectedIds.valueAt(size)) {
                    HelperMethods.transfer(getContext(), new File(videoAdapter.getItem(selectedIds.keyAt(size)).getPath()));
                }
            }
            Toast.makeText(getContext(), "Done! :)", Toast.LENGTH_SHORT).show();
            mode.finish();//Finish action mode
//                        if (mInterstitialAd.isLoaded()) {
//                            mInterstitialAd.show();
//                        } else {
//                            Log.d("TAG", "The interstitial wasn't loaded yet.");
//                        }
            return true;
        }


        return true;
    }

    @Override
    public void onDestroyActionMode(ActionMode mode) {
        videoAdapter.removeSelection();  // remove selection
        setNullToActionMode();//Set action mode null
    }
}
