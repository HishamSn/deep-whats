package com.zero1tech.whats.statussaver.util;


public interface Function<T> {
    void done(T t);
}
