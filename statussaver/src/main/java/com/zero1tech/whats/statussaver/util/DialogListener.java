package com.zero1tech.whats.statussaver.util;

public interface DialogListener {

    public void accept();
}
