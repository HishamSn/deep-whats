package com.zero1tech.whats.statussaver.util;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.provider.Settings;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.Button;

import androidx.annotation.LayoutRes;

import com.zero1tech.whats.statussaver.R;
import com.zero1tech.whats.statussaver.data.prefs.PrefsUtils;


public class DialogUtil {

    public static void showDialogEnablePermission(Activity activity) {
        AlertDialog.Builder dialog = new AlertDialog.Builder(activity);
        dialog.setMessage(activity.getResources().getString(R.string.storage_permission_not_enabled));
        dialog.setPositiveButton(activity.getResources().getString(R.string.open_settings), (paramDialogInterface, paramInt) -> {
            // TODO Auto-generated method stub
            Intent myIntent = new Intent();

            myIntent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
            myIntent.addCategory(Intent.CATEGORY_DEFAULT);
            myIntent.setData(Uri.parse("package:" + activity.getPackageName()));
            myIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            myIntent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
            myIntent.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
            activity.startActivity(myIntent);

        });

        dialog.show();
    }


    public static void alertDialog(Activity activity, @LayoutRes int layout, DialogListener dialogListener) {
//        final DriverInstruction cacheInstruction = currentInstruction;
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        final View view = activity.getLayoutInflater().inflate(layout, null);

        builder.setView(view);


        builder.setPositiveButton(R.string.ok, (dialogInterface, i) -> {
            dialogListener.accept();
        });

        builder.setNegativeButton(R.string.exit, (dialogInterface, i) -> dialogInterface.cancel());

        builder.show();
    }


    public static void loadDialogHelp(Context context) {
        View inflate = LayoutInflater.from(context).inflate(R.layout.layout_help, null);
        androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(context);
        builder.setView(inflate).setTitle(R.string.how_to_use).setPositiveButton(R.string.ok, (dialogInterface, i) -> {
            PrefsUtils.getInstance().setFirstTimeOpen(false);
            dialogInterface.dismiss();
        });
        builder.create().show();
    }

    public static void showDialog(Activity activity, DialogListener dialogListener) {
        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_permission);

        Button dialogButton = dialog.findViewById(R.id.btn_dialog);
        dialogButton.setOnClickListener(v -> {
            dialogListener.accept();
            dialog.dismiss();
        });


        dialog.show();

    }
}


