package com.zero1tech.whats.statussaver.util;

import android.content.Context;
import android.os.Environment;

import com.zero1tech.whats.statussaver.model.fileModel;

import org.apache.commons.io.comparator.LastModifiedFileComparator;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;

import static com.zero1tech.whats.statussaver.constant.AppConstant.PATH_WP_STATUSES;


public class FileUtil {


    public static File[] getFiles(String path) {
        return new File(new StringBuffer().append(Environment.getExternalStorageDirectory().getAbsolutePath()).append(PATH_WP_STATUSES).toString()).listFiles();
    }

    public static void fillImageArray(File[] listFiles, ArrayList<fileModel> imageList) {
        if (listFiles == null) {
            return;
        }

        for (File file : listFiles) {
            if (isFileImage(file)) {
                fileModel model = new fileModel(file.getAbsolutePath());
                imageList.add(model);
            }
        }
        if (!imageList.isEmpty()) {
            imageList.add(null);
        }
    }

    public static void fillVideoArray(File[] listFiles, ArrayList<fileModel> imageList) {
        if (listFiles == null) {
            return;
        }

        for (File file : listFiles) {
            if (isFileVideo(file)) {
                fileModel model = new fileModel(file.getAbsolutePath());
                imageList.add(model);
            }
        }
        if (!imageList.isEmpty()) {
            imageList.add(null);
        }

    }

    public static void saveAllFiles(Context context, File[] listFiles, Function function) {
        if (listFiles == null) {
            return;
        }
        int i = 0;
        while (i < listFiles.length) {

            File file = listFiles[i];
            if (isFileImage(file)) {
//          HelperMethods helperMethods = new HelperMethods(getActivity().getApplicationContext());
                HelperMethods.transfer(context, file);
            }
            i++;
        }

        function.done(true);
    }

    public static boolean isFileImage(File file) {
        String name = file.getName();
        return name.endsWith(".jpg") || name.endsWith(".jpeg") || name.endsWith(".png");
    }

    public static boolean isFileVideo(File file) {
        String name = file.getName();
        return name.endsWith(".mp4") || name.endsWith(".avi") || name.endsWith(".mkv") || name.endsWith(".gif");
    }

    public static void sort(File[] listFiles) {
        if (listFiles != null && listFiles.length >= 1) {
            Arrays.sort(listFiles, LastModifiedFileComparator.LASTMODIFIED_REVERSE);
        }
    }
}
