package com.zero1tech.whats.statussaver.ui.viewer;

import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.core.content.FileProvider;

import com.bumptech.glide.Glide;
import com.google.android.gms.ads.AdView;
import com.zero1tech.whats.core.BuildConfig;
import com.zero1tech.whats.core.activities.BaseActivity;
import com.zero1tech.whats.statussaver.R;
import com.zero1tech.whats.statussaver.databinding.ActivityViewerBinding;
import com.zero1tech.whats.statussaver.util.HelperMethods;

import java.io.File;

import static com.zero1tech.whats.core.util.AdsUtil.initBannerAd;
import static com.zero1tech.whats.statussaver.constant.AppConstant.EXTRA_IMAGE_VIEWER;
import static com.zero1tech.whats.statussaver.constant.AppConstant.EXTRA_VIDEO_VIEWER;


public class ViewerActivity extends BaseActivity {
    //    private InterstitialAd mInterstitialAd;
    private File file;

    private String type;
    Uri uriForFile;

    ActivityViewerBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_viewer);
        binding = ActivityViewerBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        setSupportActionBar(binding.toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        Bundle bundle = getIntent().getExtras();

        file = new File(bundle.getString("path"));
        type = bundle.getString("type");

        initAd();

        if (type.equals(EXTRA_VIDEO_VIEWER)) {
            binding.player.setVisibility(View.VISIBLE);
            binding.player.setCallback(new CustomEasyVideoCallback(this));
            binding.player.setSource(Uri.fromFile(this.file));
        }

        if (type.equals(EXTRA_IMAGE_VIEWER)) {
            binding.photo.setVisibility(View.VISIBLE);
            Glide.with(this).load(this.file).into(binding.photo);
        }

    }

    @Override
    protected void onPause() {
        super.onPause();
        if (type.equals("video")) {
            binding.player.pause();
        }
    }

    private void downloadFile() {
        try {
            HelperMethods.transfer(this, file);
            Toast.makeText(this, R.string.saved_gallery_successfully, Toast.LENGTH_SHORT).show();
//            if (mInterstitialAd.isLoaded()) {
////                        mInterstitialAd.show();
//            } else {
//                Log.d("TAG", "The interstitial wasn't loaded yet.");
//            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("GridView", new StringBuffer().append("onClick: Error: ").append(e.getMessage()).toString());
        }
    }

    private void initAd() {
//        MobileAds.initialize(getApplicationContext(), BuildConfig.AD_MOB_ID);
//        mInterstitialAd = new InterstitialAd(getApplicationContext());
//        mInterstitialAd.setAdUnitId(BuildConfig.MAIN_NATIVE_ID);
//        mInterstitialAd.loadAd(new AdRequest.Builder().build());
        AdView adView = new AdView(this);

        initBannerAd(adView, binding.containerAdView, BuildConfig.WHATS_SAVER_BANNER_ID);

    }


    public void deleteFile() {
        if (this.file.exists()) {
            this.file.delete();
            Toast.makeText(this, R.string.image_deleted, Toast.LENGTH_SHORT).show();
        }
        Intent intent = new Intent();
        setResult(-1, intent);
        finish();
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent returnIntent = new Intent();
        setResult(RESULT_CANCELED, returnIntent);
        finish();
    }

//    public InterstitialAd getmInterstitialAd() {
//        return mInterstitialAd;
//    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_image, menu);
        if (type.equals("video")) {
            MenuItem menuSetAs = menu.findItem(R.id.menu_set_as);
            menuSetAs.setVisible(false);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int itemId = item.getItemId();
        if (itemId == android.R.id.home) {
            onBackPressed();
            return true;
        } else if (itemId == R.id.menu_share) {
            Intent intent;
            getUriFile();

            intent = new Intent("android.intent.action.SEND");
            intent.setType("image/*");
            intent.setPackage("com.whatsapp");
            intent.putExtra("android.intent.extra.STREAM", uriForFile);
            startActivity(intent);
            return true;
        } else if (itemId == R.id.menu_set_as) {
            Intent intent1;
            getUriFile();

            intent1 = new Intent("android.intent.action.ATTACH_DATA");
            intent1.setDataAndType(uriForFile, "image/*");
            intent1.putExtra("mimeType", "image/*");
            startActivity(Intent.createChooser(intent1, "Set as: "));
            return true;
        } else if (itemId == R.id.menu_save) {
            downloadFile();
            return true;
        } else if (itemId == R.id.menu_delete) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage("Sure to Delete this Image?")
                    .setNegativeButton("Nope", (dialogInterface, i) -> dialogInterface.dismiss())
                    .setPositiveButton("Delete", (dialogInterface, i) -> {
                        deleteFile();
                        Toast.makeText(getApplicationContext(), "Image Deleted", Toast.LENGTH_SHORT).show();
                    });
            builder.create().show();

            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void getUriFile() {
        if (Build.VERSION.SDK_INT >= 24) {
            uriForFile = FileProvider.getUriForFile(this, getPackageName() + ".provider", file);
        } else {
            uriForFile = Uri.parse("file://" + file.getAbsolutePath());
        }
    }

}
