package com.zero1tech.whats.statussaver.ui.adapter;

import android.content.Context;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.BitmapTransitionOptions;
import com.bumptech.glide.request.RequestOptions;
import com.zero1tech.whats.statussaver.R;
import com.zero1tech.whats.statussaver.model.fileModel;

import java.util.ArrayList;

public class ImageAdapter extends RecyclerView.Adapter<ImageAdapter.ImageViewHolder> {
    private ArrayList<fileModel> arrayList;
    private Context context;
    private SparseBooleanArray selectedItemsIds;

    private static final int ROW_MORE = R.layout.row_more_data;
    private static final int ROW_IMAGE = R.layout.row_wa_image_list_item;

    public ImageAdapter(ArrayList<fileModel> arrayList) {
        this.arrayList = arrayList;
        selectedItemsIds = new SparseBooleanArray();
    }


    @Override
    public int getItemViewType(int position) {
        if (position == arrayList.size() - 1) {
            return ROW_MORE;
        }
        return ROW_IMAGE;

//        return super.getItemViewType(position);
    }

    @Override
    public int getItemCount() {
        return (null != arrayList ? arrayList.size() : 0);
    }

    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
        context = recyclerView.getContext();
    }

    @Override
    public void onBindViewHolder(ImageViewHolder holder, int position) {
        if (position == (arrayList.size() - 1)) {
            return;
        }
        //Setting text over text view
        RequestOptions centerCrop = new RequestOptions()
                .centerCrop().placeholder(R.drawable.ic_picture)
                .override(holder.imageView.getWidth(), holder.imageView.getHeight()).centerCrop();
        Glide.with(context).asBitmap().apply(centerCrop)
                .load(arrayList.get(position).getPath())
                .transition(BitmapTransitionOptions.withCrossFade())
                .into(holder.imageView);

        if (selectedItemsIds.get(position))
            holder.imageViewCheck.setVisibility(View.VISIBLE);
        else
            holder.imageViewCheck.setVisibility(View.GONE);

    }

    @Override
    public ImageViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {


        View view = LayoutInflater.from(parent.getContext()).inflate(viewType,
                parent, false);

        return new ImageViewHolder(view);

    }


    /***
     * Methods required for do selections, remove selections, etc.
     */

    //Toggle selection methods
    public void toggleSelection(int position) {
        selectView(position, !selectedItemsIds.get(position));
    }


    //Remove selected selections
    public void removeSelection() {
        selectedItemsIds = new SparseBooleanArray();
        notifyDataSetChanged();
    }


    //Put or delete selected position into SparseBooleanArray
    public void selectView(int position, boolean value) {
        if (value)
            selectedItemsIds.put(position, value);
        else
            selectedItemsIds.delete(position);

        notifyDataSetChanged();
    }

    //Get total selected count
    public int getSelectedCount() {
        return selectedItemsIds.size();
    }

    //Return all selected ids
    public SparseBooleanArray getSelectedIds() {
        return selectedItemsIds;
    }

    public fileModel getItem(int i) {
        return arrayList.get(i);
    }

    public void updateData(ArrayList<fileModel> viewModels) {
        arrayList.clear();
        arrayList.addAll(viewModels);
        notifyDataSetChanged();
    }


    public class ImageViewHolder extends RecyclerView.ViewHolder {


        public ImageView imageView, imageViewCheck;


        public ImageViewHolder(View view) {
            super(view);


            this.imageView = view.findViewById(R.id.imageView_wa_image);
            this.imageViewCheck = view.findViewById(R.id.imageView_wa_checked);

        }
    }

}