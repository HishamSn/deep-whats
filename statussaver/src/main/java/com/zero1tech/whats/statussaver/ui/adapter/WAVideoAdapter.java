package com.zero1tech.whats.statussaver.ui.adapter;

import android.content.Context;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.BitmapTransitionOptions;
import com.bumptech.glide.request.RequestOptions;
import com.zero1tech.whats.statussaver.R;
import com.zero1tech.whats.statussaver.model.fileModel;

import java.util.ArrayList;

public class WAVideoAdapter extends RecyclerView.Adapter<WAVideoAdapter.VideoViewHolder> {
    private ArrayList<fileModel> arrayList;
    private Context context;
    private SparseBooleanArray mSelectedItemsIds;
    private static final int ROW_MORE = R.layout.row_more_data;
    private static final int ROW_VIDEO = R.layout.row_video_list_item;

    public WAVideoAdapter(ArrayList<fileModel> arrayList) {
        this.arrayList = arrayList;
        mSelectedItemsIds = new SparseBooleanArray();

    }

    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
        context = recyclerView.getContext();
    }


    @Override
    public int getItemViewType(int position) {
        if (position == arrayList.size() - 1) {
            return ROW_MORE;
        }
        return ROW_VIDEO;

    }

    @Override
    public int getItemCount() {
        return (null != arrayList ? arrayList.size() : 0);

    }

    @Override
    public void onBindViewHolder(VideoViewHolder holder,
                                 int position) {
        if (position == (arrayList.size() - 1)) {
            return;
        }
        //Setting text over text view
        RequestOptions centerCrop = new RequestOptions().override(holder.imageView.getWidth(), holder.imageView.getHeight()).centerCrop();
        Glide.with(context).asBitmap().apply(centerCrop).load(arrayList.get(position).getPath()).transition(BitmapTransitionOptions.withCrossFade()).into(holder.imageView);

        if (mSelectedItemsIds.get(position)) {
            holder.imageViewCheck.setVisibility(View.VISIBLE);
            holder.imageViewPlay.setVisibility(View.GONE);
        } else {
            holder.imageViewCheck.setVisibility(View.GONE);
            holder.imageViewPlay.setVisibility(View.VISIBLE);
        }


    }

    @Override
    public VideoViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        LayoutInflater mInflater = LayoutInflater.from(viewGroup.getContext());

        View view = LayoutInflater.from(viewGroup.getContext()).inflate(viewType,
                viewGroup, false);

        return new VideoViewHolder(view);
    }


    /***
     * Methods required for do selections, remove selections, etc.
     */

    //Toggle selection methods
    public void toggleSelection(int position) {
        selectView(position, !mSelectedItemsIds.get(position));
    }


    //Remove selected selections
    public void removeSelection() {
        mSelectedItemsIds = new SparseBooleanArray();
        notifyDataSetChanged();
    }


    //Put or delete selected position into SparseBooleanArray
    public void selectView(int position, boolean value) {
        if (value)
            mSelectedItemsIds.put(position, value);
        else
            mSelectedItemsIds.delete(position);

        notifyDataSetChanged();
    }

    //Get total selected count
    public int getSelectedCount() {
        return mSelectedItemsIds.size();
    }

    //Return all selected ids
    public SparseBooleanArray getSelectedIds() {
        return mSelectedItemsIds;
    }

    public fileModel getItem(int i) {
        return (fileModel) arrayList.get(i);
    }

    public void updateData(ArrayList<fileModel> viewModels) {
        arrayList.clear();
        arrayList.addAll(viewModels);
        notifyDataSetChanged();
    }


    public class VideoViewHolder extends RecyclerView.ViewHolder {

        public ImageView imageView, imageViewCheck, imageViewPlay;

        public VideoViewHolder(View view) {
            super(view);


            this.imageView = view.findViewById(R.id.imageView_wa_image);
            this.imageViewCheck = view.findViewById(R.id.imageView_wa_checked);
            this.imageViewPlay = view.findViewById(R.id.imageView_wa_play);

        }
    }


}