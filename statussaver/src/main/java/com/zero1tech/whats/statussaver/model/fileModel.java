package com.zero1tech.whats.statussaver.model;


public class fileModel {
    private String path;
    private boolean isSelected = false;

    public fileModel(String path) {
        this.path = path;
    }

    public String getPath() {
        return path;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }


    public boolean isSelected() {
        return isSelected;
    }

}
