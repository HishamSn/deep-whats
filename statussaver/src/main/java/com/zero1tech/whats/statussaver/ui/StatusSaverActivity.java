package com.zero1tech.whats.statussaver.ui;


import android.Manifest.permission;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.viewpager2.widget.ViewPager2;

import com.google.android.gms.ads.AdView;
import com.google.android.material.navigation.NavigationView;
import com.google.android.material.tabs.TabLayout;
import com.google.android.material.tabs.TabLayoutMediator;
import com.zero1tech.whats.core.BuildConfig;
import com.zero1tech.whats.core.activities.BaseActivity;
import com.zero1tech.whats.statussaver.R;
import com.zero1tech.whats.statussaver.data.prefs.PrefsUtils;
import com.zero1tech.whats.statussaver.ui.adapter.MainViewPagerAdapter;
import com.zero1tech.whats.statussaver.util.DialogUtil;
import com.zero1tech.whats.statussaver.util.Function;

import java.io.File;

import hotchemi.android.rate.AppRate;

import static com.zero1tech.whats.core.util.AdsUtil.initBannerAd;
import static com.zero1tech.whats.statussaver.constant.AppConstant.REQUEST_WRITE_EXTERNAL_STORAGE;
import static com.zero1tech.whats.statussaver.util.DialogUtil.loadDialogHelp;
import static com.zero1tech.whats.statussaver.util.DialogUtil.showDialogEnablePermission;


public class StatusSaverActivity extends BaseActivity
        implements NavigationView.OnNavigationItemSelectedListener {


    private NavigationView navigationView;
    private Function<Boolean> permissionStorageListener;
    private MainViewPagerAdapter mainViewPagerAdapter;
    private ViewPager2 viewPager;
    private TabLayout tabLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_status_saver);
        init();

        createFolders();
        navigationView.getMenu().getItem(0).setChecked(true);


        permissionStorageListener = genrated -> {
            if (genrated) {
                initFragments();
            }
        };

        requestPermissionStorage(permissionStorageListener);

        initAds(this);

    }

    private void initAds(Context ctx) {
        if (!BuildConfig.isAdsBlocked) {
            AdView adView = new AdView(ctx);
            initBannerAd(adView, findViewById(R.id.container_ad_view), BuildConfig.WHATS_SAVER_BANNER_ID);
        }
    }


    private void checkIsFirstTimeOpen() {
        if (PrefsUtils.getInstance().isFirstTimeOpen()) {
            loadDialogHelp(this);
        }
    }

    private void activateImageVideoFragment() {
        viewPager.setAdapter(mainViewPagerAdapter);

        new TabLayoutMediator(tabLayout, viewPager, (tab, position) -> {
            if (position == 0) {
                tab.setText(R.string.Images);
            } else if (position == 1) {
                tab.setText(R.string.Videos);
            }
        }).attach();

        viewPager.setOffscreenPageLimit(2);
    }

    private void initFragments() {

        activateImageVideoFragment();
        checkIsFirstTimeOpen();

    }

    private void init() {

        Toolbar toolbar = findViewById(R.id.toolbar);
        tabLayout = findViewById(R.id.tab_layout_wa);
        viewPager = findViewById(R.id.viewPager_wa);

        setSupportActionBar(toolbar);

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);

        drawer.addDrawerListener(toggle);
        toggle.syncState();
        navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        mainViewPagerAdapter = new MainViewPagerAdapter(this);

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.menu_share) {
            Intent intent = new Intent("android.intent.action.SEND");
            intent.setType("text/plain");
            intent.putExtra("android.intent.extra.SUBJECT", getString(R.string.share_wp_via_desc));
            intent.putExtra("android.intent.extra.TEXT", getString(R.string.share_app));
            startActivity(Intent.createChooser(intent, getString(R.string.share_via)));
            return true;
        }

        if (id == R.id.action_help) {
            loadDialogHelp(this);
        }

        return super.onOptionsItemSelected(item);
    }


    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_whatsapp) {
//            Fragment fragment = new MainFragment();
//            FragmentManager fm = getSupportFragmentManager();
//            fm.beginTransaction().replace(R.id.framelayout, fragment).commit();
        }
        // Handle the camera action
        else if (id == R.id.nav_business) {
            if (checkInstallation("com.whatsapp.w4b")) {
//                Fragment fragment = new BWAFragment();
//                FragmentManager fm = getSupportFragmentManager();
//                fm.beginTransaction().replace(R.id.framelayout, fragment).commit();
            } else {
                Toast.makeText(this, "Business Whatsapp Not Installed", Toast.LENGTH_SHORT).show();
            }

        } else if (id == R.id.nav_language) {

        } else {
            AppRate.with(this).showRateDialog(this);
        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void createFolders() {
        StringBuilder stringBuffer = new StringBuilder().append(new StringBuffer().append(Environment.getExternalStorageDirectory()).append(File.separator).toString());
        File file = new File(stringBuffer.append("WhatsApp/Media/.Statuses").toString());
        if (!file.isDirectory()) {
            file.mkdirs();
        }
        file = new File(stringBuffer.append("WhatsApp Business/Media/.Statuses").toString());
        if (!file.isDirectory()) {
            file.mkdirs();
        }
//        file = new File(stringBuffer.append("GBWhatsApp/Media/.Statuses").toString());
//        if (!file.isDirectory()) {
//            file.mkdirs();
//        }
        new File(stringBuffer.append("StorySaver/").toString())
                .mkdirs();
    }

    private boolean checkInstallation(String uri) {
        PackageManager pm = getPackageManager();
        boolean app_installed = false;
        try {
            pm.getPackageInfo(uri, PackageManager.GET_ACTIVITIES);
            app_installed = true;
        } catch (PackageManager.NameNotFoundException e) {
            app_installed = false;
        }
        return app_installed;
    }

    public void requestPermissionStorage(Function<Boolean> permissionGenerated) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (isReadWritePermissionEnabled()) {
                DialogUtil.showDialog(this, () -> {
                    requestPermissions(new String[]{permission.WRITE_EXTERNAL_STORAGE, permission.WRITE_EXTERNAL_STORAGE}, 1);
                    permissionGenerated.done(false);
                });
                return;
            }
        }
        permissionGenerated.done(true);

    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private boolean isReadWritePermissionEnabled() {
        return getApplicationContext().checkSelfPermission(permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED &&
                getApplicationContext().checkSelfPermission(permission.WRITE_EXTERNAL_STORAGE) !=
                        PackageManager.PERMISSION_GRANTED;
    }


    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {

        if (requestCode != REQUEST_WRITE_EXTERNAL_STORAGE) {
            return;
        }

        // If request is cancelled, the result arrays are empty.
        if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            permissionStorageListener.done(true);

        } else if (ActivityCompat.shouldShowRequestPermissionRationale(this, permission.WRITE_EXTERNAL_STORAGE)) {
            requestPermissionStorage(genrated -> {

            });
        } else {
            showDialogEnablePermission(this);
        }

    }

}

