package com.zero1tech.whats.statussaver.model;


public class LanguageModel {


    private Integer id;
    private String languageName;
    private String iconUrl;
    private String key;

    public LanguageModel() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }


    public String getIconUrl() {
        return iconUrl;
    }

    public void setIconUrl(String iconUrl) {
        this.iconUrl = iconUrl;
    }
}
