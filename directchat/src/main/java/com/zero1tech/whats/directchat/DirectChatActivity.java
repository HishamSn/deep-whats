package com.zero1tech.whats.directchat;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.view.Gravity;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;

import com.hbb20.CountryCodePicker;
import com.zero1tech.whats.core.activities.BaseActivity;

public class DirectChatActivity extends BaseActivity {

    private EditText mPhoneNumber;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_direct_chat);

    }

    public void contactOnWhatsApp(View v) {

        EditText phoneNumberField = findViewById(R.id.inputField);
        if (phoneNumberField.getText().toString().isEmpty()) {
            Alert_Dialog_Blank_Input();
        } else {
            //  CODE FOR COUNTRY CODE SPINNER
            CountryCodePicker cpp = findViewById(R.id.cpp);
            cpp.registerCarrierNumberEditText(phoneNumberField);
            String phoneNumber = cpp.getFullNumber();


            boolean installed = appInstalledOrNot("com.whatsapp");
            if(installed) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://api.whatsapp.com/send?phone=" + phoneNumber));
                startActivity(browserIntent);
            } else {
                Toast.makeText(this,"Whatsapp is not installed on your device",Toast.LENGTH_SHORT).show();
            }

        }
    }

    public void SaveContact(View v) {

        EditText phoneNumberField = findViewById(R.id.inputField);
        if (phoneNumberField.getText().toString().isEmpty()) {
            Alert_Dialog_Blank_Input();
        } else {
            Intent intent = new Intent(ContactsContract.Intents.Insert.ACTION);
            intent.setType(ContactsContract.RawContacts.CONTENT_TYPE);
            mPhoneNumber = findViewById(R.id.inputField);

            CountryCodePicker cpp = findViewById(R.id.cpp);
            String fullNumber = cpp.getFullNumberWithPlus() + mPhoneNumber.getText().toString();
            intent.putExtra(ContactsContract.Intents.Insert.PHONE, fullNumber);

            startActivity(intent);
        }

    }


    //Function to clear numbers
    public void clearNumber(View view) {
        EditText hello = findViewById(R.id.inputField);
        hello.setText("");
    }

    private boolean appInstalledOrNot(String uri) {
        PackageManager pm = getPackageManager();
        boolean app_installed;
        try {
            pm.getPackageInfo(uri, PackageManager.GET_ACTIVITIES);
            app_installed = true;
        }
        catch (PackageManager.NameNotFoundException e) {
            app_installed = false;
        }
        return app_installed;
    }


    public void Alert_Dialog_Blank_Input() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(true);
        builder.setTitle(R.string.error);
        builder.setMessage(R.string.please_enter_a_number);
        AlertDialog alert = builder.create();
        alert.getWindow().setGravity(Gravity.CENTER);
        alert.show();
    }

}
