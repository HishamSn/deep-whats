package com.zero1tech.whats.tools.calllog;

import android.database.Cursor;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.loader.app.LoaderManager;
import androidx.loader.content.Loader;

public class CallLogActivity extends AppCompatActivity implements
        LoaderManager.LoaderCallbacks<Cursor> {

    private static final String ARG_PHONE_NUMBER = "phone_number";
    private static final String ARG_CONTACT_NAME = "contact_name";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_call_log);

    }

    @NonNull
    @Override
    public Loader<Cursor> onCreateLoader(int id, @Nullable Bundle args) {
        String contactName = null;
        String phoneNumber = null;
        if (args != null && args.containsKey(ARG_PHONE_NUMBER)) {
            phoneNumber = args.getString(ARG_PHONE_NUMBER);
        }
        if (args != null && args.containsKey(ARG_CONTACT_NAME)) {
            contactName = args.getString(ARG_CONTACT_NAME);
        }

        RecentsCursorLoader recentsCursorLoader = new RecentsCursorLoader(this, phoneNumber, contactName);
        return recentsCursorLoader;
    }

    @Override
    public void onLoadFinished(@NonNull Loader<Cursor> loader, Cursor data) {
        setData(data);

    }

    @Override
    public void onLoaderReset(@NonNull Loader<Cursor> loader) {
//        mRecentsAdapter.changeCursor(null);

    }


    private void setData(Cursor data) {
//        mRecentsAdapter.changeCursor(data);
//        mFastScroller.setup(mRecentsAdapter, mLayoutManager);
//        if (mRefreshLayout.isRefreshing()) mRefreshLayout.setRefreshing(false);
//        if (data != null && data.getCount() > 0) {
//            mRecyclerView.setVisibility(View.VISIBLE);
//            mEmptyState.setVisibility(GONE);
//        } else {
//            mRecyclerView.setVisibility(GONE);
//            mEmptyState.setVisibility(View.VISIBLE);
//        }
    }
}
