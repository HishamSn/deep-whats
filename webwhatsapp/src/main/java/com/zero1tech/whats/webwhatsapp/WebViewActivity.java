package com.zero1tech.whats.webwhatsapp;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.webkit.PermissionRequest;
import android.webkit.ValueCallback;
import android.webkit.WebStorage;
import android.webkit.WebView;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.SwitchCompat;
import androidx.appcompat.widget.Toolbar;

import com.google.android.gms.ads.AdView;
import com.zero1tech.whats.core.BuildConfig;
import com.zero1tech.whats.core.activities.BaseActivity;
import com.zero1tech.whats.webwhatsapp.util.CustomWebChromeClient;
import com.zero1tech.whats.webwhatsapp.util.CustomWebViewClient;

import java.util.Arrays;

import static com.zero1tech.whats.core.util.AdsUtil.initBannerAd;
import static com.zero1tech.whats.webwhatsapp.constant.AppConstant.AUDIO_PERMISSION_RESULTCODE;
import static com.zero1tech.whats.webwhatsapp.constant.AppConstant.CAMERA_PERMISSION_RESULTCODE;
import static com.zero1tech.whats.webwhatsapp.constant.AppConstant.DEBUG_TAG;
import static com.zero1tech.whats.webwhatsapp.constant.AppConstant.FILECHOOSER_RESULTCODE;
import static com.zero1tech.whats.webwhatsapp.constant.AppConstant.STORAGE_PERMISSION_RESULTCODE;
import static com.zero1tech.whats.webwhatsapp.constant.AppConstant.USER_AGENT;
import static com.zero1tech.whats.webwhatsapp.constant.AppConstant.VIDEO_PERMISSION_RESULTCODE;
import static com.zero1tech.whats.webwhatsapp.constant.AppConstant.WHATSAPP_WEB_URL;
import static com.zero1tech.whats.webwhatsapp.util.webViewUtil.configureSetting;

public class WebViewActivity extends BaseActivity {

    private final Activity activity = this;
    private WebView mWebView;
    private ViewGroup mMainView;

    private long mLastBackClick = 0;


    private ValueCallback<Uri[]> mUploadMessage;
    private PermissionRequest mCurrentPermissionRequest;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_whats);
        Toolbar toolbar = findViewById(R.id.toolbar);
        initAds(this);

        mMainView = findViewById(R.id.layout);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
            setdirCache();
        }
        mWebView = findViewById(R.id.webview);

        // webview stuff
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        configureSetting(mWebView, USER_AGENT);


        mWebView.setWebChromeClient(new CustomWebChromeClient(this, mCurrentPermissionRequest, mUploadMessage));

        mWebView.setWebViewClient(new CustomWebViewClient(this));

        if (savedInstanceState == null) {
            loadWhatsapp();
        } else {
            Log.d(DEBUG_TAG, "savedInstanceState is present");
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.P)
    private void setdirCache() {
        String process = getProcessName(this);
        if (!getPackageName().equals(process)) {
            WebView.setDataDirectorySuffix(process);
        }
    }


    private void initAds(Context ctx) {
        if (!BuildConfig.isAdsBlocked) {
            AdView adView = new AdView(ctx);
            initBannerAd(adView, findViewById(R.id.container_ad_view), BuildConfig.WEB_WHATS_BANNER_ID);
        }
    }

    public static String getProcessName(Context context) {
        ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningAppProcessInfo processInfo : manager.getRunningAppProcesses()) {
            if (processInfo.pid == android.os.Process.myPid()) {
                return processInfo.processName;
            }
        }

        return null;
    }

    @Override
    protected void onResume() {
        super.onResume();
        mWebView.onResume();
//        setNavbarEnabled(PrefsUtils.getInstance().isNavbarEnabled());

    }

    @Override
    protected void onPause() {
        super.onPause();
        mWebView.onPause();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.action_bar, menu);

        SwitchCompat switchKeyboardEnable = menu.findItem(R.id.action_switch_enable_keyboard)
                .getActionView().findViewById(R.id.switch_keyboard_enable);

        switchKeyboardEnable.setOnCheckedChangeListener((compoundButton, isChecked) -> {
            setKeyboardEnabled(isChecked);
        });

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int itemId = item.getItemId();
        if (itemId == R.id.toggle_keyboard) {
            toggleKeyboard();

        } else if (itemId == R.id.menu_logout) {//                showToast("scroll left");
//                runOnUiThread(() -> mWebView.scrollTo(0, 0));
            showSnackbar("logging out...");
            mWebView.loadUrl("javascript:localStorage.clear()");
            WebStorage.getInstance().deleteAllData();
            loadWhatsapp();
        } else if (itemId == R.id.menu_reload) {//                showToast("scroll right");
//                runOnUiThread(() -> mWebView.scrollTo(2000, 0));
//                mWebView.setInitialScale(50);

            showSnackbar("reloading...");
            loadWhatsapp();
        }
        return true;
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case VIDEO_PERMISSION_RESULTCODE:
                if (permissions.length == 2 && grantResults[0] == PackageManager.PERMISSION_GRANTED &&
                        grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                    try {
                        mCurrentPermissionRequest.grant(mCurrentPermissionRequest.getResources());
                    } catch (RuntimeException e) {
                        Log.e(DEBUG_TAG, "Granting permissions failed", e);
                    }
                } else {
                    showSnackbar("Permission not granted, can't use video.");
                    mCurrentPermissionRequest.deny();
                }
                break;
            case CAMERA_PERMISSION_RESULTCODE:
            case AUDIO_PERMISSION_RESULTCODE:
                //same same
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    try {
                        mCurrentPermissionRequest.grant(mCurrentPermissionRequest.getResources());
                    } catch (RuntimeException e) {
                        Log.e(DEBUG_TAG, "Granting permissions failed", e);
                    }
                } else {
                    showSnackbar("Permission not granted, can't use " +
                            (requestCode == CAMERA_PERMISSION_RESULTCODE ? "camera" : "microphone"));
                    mCurrentPermissionRequest.deny();
                }
                break;
            case STORAGE_PERMISSION_RESULTCODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // TODO: check for current download and enqueue it
                } else {
                    showSnackbar("Permission not granted, can't download to storage");
                }
                break;
            default:
                Log.d(DEBUG_TAG, "Got permission result with unknown request code " +
                        requestCode + " - " + Arrays.asList(permissions).toString());
        }
        mCurrentPermissionRequest = null;
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mWebView.saveState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        mWebView.restoreState(savedInstanceState);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == FILECHOOSER_RESULTCODE) {
            if (resultCode == RESULT_CANCELED || data.getData() == null) {
                mUploadMessage.onReceiveValue(null);
            } else {
                Uri result = data.getData();
                Uri[] results = new Uri[1];
                results[0] = result;
                mUploadMessage.onReceiveValue(results);
            }
        } else {
            Log.d(DEBUG_TAG, "Got activity result with unknown request code " +
                    requestCode + " - " + data.toString());
        }
    }

    private void toggleKeyboard() {
        setKeyboardEnabled(!PrefsUtils.getInstance().isKeyboardEnabled());
    }

    private void setKeyboardEnabled(final boolean enable) {
        InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        if (enable && mMainView.getDescendantFocusability() == ViewGroup.FOCUS_BLOCK_DESCENDANTS) {
            mMainView.setDescendantFocusability(ViewGroup.FOCUS_AFTER_DESCENDANTS);
            showSnackbar("Unblocking keyboard...");
            //inputMethodManager.showSoftInputFromInputMethod(activity.getCurrentFocus().getWindowToken(), 0);
        } else if (!enable) {
            mMainView.setDescendantFocusability(ViewGroup.FOCUS_BLOCK_DESCENDANTS);
            mWebView.getRootView().requestFocus();
            showSnackbar("Blocking keyboard...");
            inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
        }
        PrefsUtils.getInstance().setKeyboardEnabled(enable);
    }

    @Override
    public void onBackPressed() {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(R.string.back_home_screen_question)
                .setNegativeButton(R.string.No, (dialogInterface, i) -> dialogInterface.dismiss())
                .setPositiveButton(R.string.yes, (dialogInterface, i) -> {
                    mWebView.dispatchKeyEvent(new KeyEvent(KeyEvent.ACTION_DOWN, KeyEvent.KEYCODE_ESCAPE));
                    finish();
                });
        builder.create().show();
    }

    private void loadWhatsapp() {
        mWebView.getSettings().setUserAgentString(USER_AGENT);
        mWebView.loadUrl(WHATSAPP_WEB_URL);
    }

}
