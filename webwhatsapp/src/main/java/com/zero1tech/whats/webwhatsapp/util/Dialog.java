package com.zero1tech.whats.webwhatsapp.util;

import android.app.AlertDialog;
import android.content.Context;

import androidx.annotation.StringRes;

public class Dialog {

    public static void showDialogInfo(Context context,@StringRes int resourcesId) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage(resourcesId)
                .setCancelable(false)
                .setPositiveButton("Ok", null);
        AlertDialog alert = builder.create();
        alert.show();
    }
}
