package com.zero1tech.whats.webwhatsapp;


import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.zero1tech.whats.core.MyApplication;


public class PrefsUtils {

    private static final String LOGIN = "login";
    private static final String LANGUAGE_SELECTED = "language_selected";
    private static final String KEYBOARD_ENABLED = "keyboardEnabled";
    private static final String NAVBAR_ENABLED = "navbarEnabled";
    private static final String INTRO_SHOW = "introShown";
    private static final String LAST_SHOWN_VERSION_CODE = "lastShownVersionCode";
    private static PrefsUtils instance;
    private SharedPreferences prefs;

    private PrefsUtils() {
        prefs = PreferenceManager.getDefaultSharedPreferences(MyApplication.getInstance());
    }

    public static synchronized PrefsUtils getInstance() {
        if (instance == null) {
            instance = new PrefsUtils();
        }
        return instance;
    }

    public boolean isLogin() {
        return prefs.getBoolean(LOGIN, false);
    }

    public void setLogin(boolean login) {
        prefs.edit().putBoolean(LOGIN, login).apply();
    }

    public boolean isKeyboardEnabled() {
        return prefs.getBoolean(KEYBOARD_ENABLED, true);
    }

    public void setKeyboardEnabled(boolean enable) {
        prefs.edit().putBoolean(KEYBOARD_ENABLED, enable).apply();
    }

    public boolean isNavbarEnabled() {
        return prefs.getBoolean(NAVBAR_ENABLED, true);
    }

    public void setNavbarEnabled(boolean enable) {
        prefs.edit().putBoolean(NAVBAR_ENABLED, enable).apply();
    }

    public boolean isIntroShow() {
        return prefs.getBoolean(INTRO_SHOW, false);
    }

    public void setIntroShow(boolean enable) {
        prefs.edit().putBoolean(INTRO_SHOW, enable).apply();
    }

    public Integer getLastShownVersionCode() {
        return prefs.getInt(LAST_SHOWN_VERSION_CODE, 0);
    }

    public void setLastShownVersionCode(int version) {
        prefs.edit().putInt(LAST_SHOWN_VERSION_CODE, version).apply();
    }




}
