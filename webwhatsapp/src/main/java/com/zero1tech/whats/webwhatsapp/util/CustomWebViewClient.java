package com.zero1tech.whats.webwhatsapp.util;

import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.util.Log;
import android.view.KeyEvent;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import androidx.annotation.RequiresApi;

import static com.zero1tech.whats.webwhatsapp.constant.AppConstant.DEBUG_TAG;
import static com.zero1tech.whats.webwhatsapp.constant.AppConstant.WEB_WHATSAPP_URL;

public class CustomWebViewClient extends WebViewClient {
    private Activity activity;

    public CustomWebViewClient(Activity activity) {
        this.activity = activity;
    }

    public void onPageFinished(WebView view, String url) {
        view.scrollTo(0, 0);
    }

    @Override
    public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
        final String url = request.getUrl().toString();
        Log.d(DEBUG_TAG, url);

        if (url.contains(WEB_WHATSAPP_URL)) {
            // whatsapp web request -> fine
            return super.shouldOverrideUrlLoading(view, request);
        } else {
            Intent intent = new Intent(Intent.ACTION_VIEW, request.getUrl());
            activity.startActivity(intent);
            return true;
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
        String msg = String.format("Error: %s - %s", error.getErrorCode(), error.getDescription());
        Log.d(DEBUG_TAG, msg);
    }

    public void onUnhandledKeyEvent(WebView view, KeyEvent event) {
        Log.d(DEBUG_TAG, "Unhandled key event: " + event.toString());
    }
}
