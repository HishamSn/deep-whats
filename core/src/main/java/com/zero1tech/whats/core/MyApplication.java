package com.zero1tech.whats.core;

import android.app.Application;
import android.util.Log;

import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.initialization.InitializationStatus;
import com.google.android.gms.ads.initialization.OnInitializationCompleteListener;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;
import com.google.firebase.analytics.FirebaseAnalytics;


public class MyApplication extends Application {

    private static MyApplication instance;
    private static GoogleAnalytics sAnalytics;
    private static Tracker sTracker;

    private FirebaseAnalytics FirebaseAnalytics;
//    FirebaseRemoteConfig firebaseRemoteConfig;

    public static MyApplication getInstance() {
        return instance;
    }


    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
//        LocalHelper.onAttach(this,"es");
//        Pandora.get().getInterceptor();
        FirebaseAnalytics = FirebaseAnalytics.getInstance(MyApplication.getInstance());
        sAnalytics = GoogleAnalytics.getInstance(this);
        MobileAds.initialize(this, BuildConfig.AD_MOB_ID);

        MobileAds.initialize(this, new OnInitializationCompleteListener() {
            @Override
            public void onInitializationComplete(InitializationStatus initializationStatus) {
                Log.d("MyApplication",initializationStatus.toString());
            }
        });

//        firebaseRemoteConfig = FirebaseRemoteConfig.getInstance();

//        MobileAds.initialize(this, BuildConfig.AD_MOB_ID);


        registerActivityLifecycleCallbacks(new LifecycleCallbacks());
    }

//    public FirebaseRemoteConfig getFirebaseRemoteConfig() {
//        return firebaseRemoteConfig;
//    }

    synchronized public Tracker getDefaultTracker() {
        // To enable debug logging use: adb shell setprop log.tag.GAv4 DEBUG
        if (sTracker == null) {
            sTracker = sAnalytics.newTracker(R.xml.global_tracker);
        }

        return sTracker;
    }
}