package com.zero1tech.whats.core;

import android.app.Activity;
import android.app.Application;
import android.os.Bundle;
import android.util.Log;

public final class LifecycleCallbacks implements Application.ActivityLifecycleCallbacks {
    @Override
    public void onActivityCreated(Activity activity, Bundle savedInstanceState) {
        Log.i("Tracking_Activity", "Created : " + activity.getLocalClassName());
    }

    @Override
    public void onActivityStarted(Activity activity) {
        Log.i("Tracking_Activity", "Started : " + activity.getLocalClassName());
        //do nothing
    }

    @Override
    public void onActivityResumed(Activity activity) {
//            Adjust.onResume();
        Log.i("Tracking_Activity", "Resumed : " + activity.getLocalClassName());

    }

    @Override
    public void onActivityPaused(Activity activity) {
        Log.i("Tracking_Activity", "Paused : " + activity.getLocalClassName());
    }

    @Override
    public void onActivityStopped(Activity activity) {
        Log.i("Tracking_Activity", "Stopped : " + activity.getLocalClassName());
    }

    @Override
    public void onActivitySaveInstanceState(Activity activity, Bundle outState) {
        Log.i("Tracking_Activity", "SaveInstanceState : " + activity.getLocalClassName());
    }

    @Override
    public void onActivityDestroyed(Activity activity) {
        Log.i("Tracking_Activity", "Destroyed : " + activity.getLocalClassName());

    }



}