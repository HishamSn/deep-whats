package com.zero1tech.whats.core.util;

import android.content.Context;
import android.util.Log;
import android.widget.LinearLayout;

import androidx.cardview.widget.CardView;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.zero1tech.whats.core.BuildConfig;

public class AdsUtil {


    public static InterstitialAd initInterstitialAd(Context context, InterstitialAd ad) {
        ad = new InterstitialAd(context);
        ad.setAdUnitId(BuildConfig.MAIN_NATIVE_ID);
        ad.loadAd(new AdRequest.Builder().build());
        return ad;
    }

    public static AdView initBannerAd(AdView ad, LinearLayout containerAdView, String unit) {
//        ad = new AdView(context);
        ad.setAdUnitId(unit);
        ad.setAdSize(AdSize.BANNER);
        containerAdView.addView(ad);
        AdRequest adRequest = new AdRequest.Builder().build();
        ad.loadAd(adRequest);
        ad.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                // Code to be executed when an ad finishes loading.
                Log.d("initBannerAd","sucess");

            }

            @Override
            public void onAdFailedToLoad(int errorCode) {
                Log.d("initBannerAd",errorCode+"");
                //                    new Handler().postDelayed(() -> {
//                    // Code to be executed when an ad request fails.
//                    AdRequest adRequest = new AdRequest.Builder().build();
//                    adView.loadAd(adRequest);
//                    }, 10000);

//                if (isConnected(context)){
//                startMyAds(context, cardView);}

            }

            @Override
            public void onAdOpened() {
                // Code to be executed when an ad opens an overlay that
                // covers the screen.


            }

            @Override
            public void onAdClicked() {
                // Code to be executed when the user clicks on an ad.
//                Bundle bundle = new Bundle();
//                bundle.putString("email", email);
//                MyApplication.getInstance().getFirebaseAnalytics().logEvent("try_login", bundle);


            }

            @Override
            public void onAdLeftApplication() {
                // Code to be executed when the user has left the app.

            }

            @Override
            public void onAdClosed() {
                // Code to be executed when the user is about to return
                // to the app after tapping on an ad.

            }

        });

        return ad;
    }


    public static void BannerAds(Context context, AdView adView, CardView cardView) {

        AdRequest adRequest = new AdRequest.Builder()
                .build();
        adView.loadAd(adRequest);
        adView.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                // Code to be executed when an ad finishes loading.

            }

            @Override
            public void onAdFailedToLoad(int errorCode) {
                //                    new Handler().postDelayed(() -> {
//                    // Code to be executed when an ad request fails.
//                    AdRequest adRequest = new AdRequest.Builder().build();
//                    adView.loadAd(adRequest);
//                    }, 10000);

//                if (isConnected(context)){
//                startMyAds(context, cardView);}

            }

            @Override
            public void onAdOpened() {
                // Code to be executed when an ad opens an overlay that
                // covers the screen.


            }

            @Override
            public void onAdClicked() {
                // Code to be executed when the user clicks on an ad.
//                Bundle bundle = new Bundle();
//                bundle.putString("email", email);
//                MyApplication.getInstance().getFirebaseAnalytics().logEvent("try_login", bundle);


            }

            @Override
            public void onAdLeftApplication() {
                // Code to be executed when the user has left the app.

            }

            @Override
            public void onAdClosed() {
                // Code to be executed when the user is about to return
                // to the app after tapping on an ad.

            }
        });


    }

}

