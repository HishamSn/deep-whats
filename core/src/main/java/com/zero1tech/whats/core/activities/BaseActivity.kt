package com.zero1tech.whats.core.activities

import android.R
import android.annotation.SuppressLint
import android.graphics.Color
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.snackbar.Snackbar

@SuppressLint("Registered")
abstract class BaseActivity : AppCompatActivity() {


//    override fun attachBaseContext(base: Context?) {
//        super.attachBaseContext(LocalHelper.onAttach(base,"es"))
//    }

    fun toast(msgRes: Int) {
        toast(getString(msgRes))
    }

    fun toast(msg: String) {
        Toast.makeText(this, msg, Toast.LENGTH_LONG).show()
    }

    open fun showSnackbar(msg: String?) {
        val snackbar = Snackbar.make(findViewById(R.id.content), msg!!, 900)
        snackbar.setAction("dismiss") { view: View? -> snackbar.dismiss() }
        snackbar.setActionTextColor(Color.parseColor("#075E54"))
    }

}